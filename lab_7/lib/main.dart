import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Login Page",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

// showAlertDialog(BuildContext context) {
//   // Create button
//   Widget okButton = FlatButton(
//     child: Text("OK"),
//     onPressed: () {
//       Navigator.of(context).pop();
//     },
//   );

//   // Create AlertDialog
//   AlertDialog alert = AlertDialog(
//     title: Text("Login Successful!"),
//     // content: Text(""),
//     actions: [
//       okButton,
//     ],
//   );

//   // show the dialog
//   showDialog(
//     context: context,
//     builder: (BuildContext context) {
//       return alert;
//     },
//   );
// }

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  String _uname = "";
  String gabungan = "";
  bool ada = false;

  showAlertDialog(BuildContext context, String _uname) {
    // Create button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // Create AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Login Successful! Nice to meet you " + _uname + "!"),
      // content: Text(""),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void baru(String nama) {
    setState(() {
      _uname = nama;
      if (_uname == "") {
        ada = false;
      } else {
        ada = true;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("LOG IN"),
        backgroundColor: Colors.indigo,
      ),
      body: Form(
        key: _formKey,
        // child: Container(
        //   padding: const EdgeInsets.only(top: 120, left: 24, right: 24),
        //   decoration: BoxDecoration(
        //       image: DecorationImage(
        //     image: AssetImage("assets/images/libr.jpeg"),
        //     fit: BoxFit.cover,
        //   )),
        child: Center(
          child: Column(
            children: [
              Padding(padding: EdgeInsets.only(top: 40.0)),
              Text(
                'Welcome Back!',
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(height: 20),
              Column(
                children: [
                  Padding(padding: EdgeInsets.all(10.0)),
                  // TextField(),
                  TextFormField(
                    decoration: new InputDecoration(
                      // fillColor: Color(0xffF1F0F5),
                      hintText: "contoh: fasya.prandari",
                      labelText: "Username",
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    onChanged: (String value) {
                      baru(value);
                    },
                    validator: (value) {
                      if (value != null && value.isEmpty) {
                        return 'Nama tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                  Padding(padding: EdgeInsets.all(10.0)),
                  TextFormField(
                    decoration: new InputDecoration(
                      // fillColor: Color(0xffF1F0F5),
                      // hintText: "contoh: Budidudi",
                      labelText: "Password",
                      icon: Icon(Icons.lock),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0)),
                    ),
                    validator: (value) {
                      if (value != null && value.isEmpty) {
                        return 'Password tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                  Padding(padding: EdgeInsets.all(10.0)),
                  RaisedButton(
                    child: Text(
                      "Submit",
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Colors.indigo,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    onPressed: () {
                      if (_formKey.currentState != null &&
                          _formKey.currentState!.validate()) {
                        showAlertDialog(context, _uname);
                        print(_uname + " berhasil login!");
                      }
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      // ),
    );
  }
}
