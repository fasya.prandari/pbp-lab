from django.shortcuts import render
from lab_1.models import Friend
from lab_3.forms import FriendForm
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.
#source: geeksforgeeks

@login_required(login_url = '/admin/login/')
def index(request):
    isiFriends = Friend.objects.all().values()  # TODO Implement this
    value = {'friends': isiFriends}
    return render(request, 'lab3_index.html', value)

@login_required(login_url = '/admin/login/')
def add_friend(request):
    value = {}
    isiForm = FriendForm(request.POST or None, request.FILES or None)
    
    if (isiForm.is_valid and request.method == "POST"):    #cek if valid datanya n method sama
        isiForm.save()
        return HttpResponseRedirect('/lab-3')   #redirect url yg bisa dibuka
    
    value['form'] = isiForm
    return render(request, 'lab3_form.html', value)