from django import forms
from django.db.models import fields
from lab_1.models import Friend

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here, data base

class FriendForm(forms.ModelForm):
   class Meta:
       model = Friend
       fields = "__all__"
    # TODO Implement missing attributes in Friend model
