from django.urls import path
from .views import add_note, index, note_list

urlpatterns = [
    path('', index, name='index'),
    path('add_note', add_note, name='add_note'),
    path('note_list', note_list, name='note_list'),
]