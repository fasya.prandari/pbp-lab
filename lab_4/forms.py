from django import forms
from django.db.models import fields
from lab_2.models import Note

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here, data base

class NoteForm(forms.ModelForm):
   class Meta:
       model = Note
       fields = "__all__"

