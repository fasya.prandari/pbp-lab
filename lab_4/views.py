from django.shortcuts import render
from lab_2.models import Note
from lab_4.forms import NoteForm
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.

def index(request):
    notes = Note.objects.all()
    response = {'note': notes}
    return render(request, 'lab4_index.html', response)

@login_required(login_url = '/admin/login/')
def add_note(request):
    value = {}
    isiForm = NoteForm(request.POST or None, request.FILES or None)
    
    if (isiForm.is_valid and request.method == "POST"):    #cek if valid datanya n method sama
        isiForm.save()
        return HttpResponseRedirect('/lab-4')   #redirect url yg bisa dibuka
    
    value['form'] = isiForm
    return render(request, 'lab4_form.html', value)

def note_list(request):
    notes = Note.objects.all()
    response = {'note': notes}
    return render(request, 'lab4_note_list.html', response)