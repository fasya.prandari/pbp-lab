from django.db import models

class Note(models.Model):
    To = models.CharField(max_length=50)
    From = models.CharField(max_length=50)
    Title = models.CharField(max_length=100)
    Message = models.CharField(max_length=500)
