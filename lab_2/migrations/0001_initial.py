# Generated by Django 3.2.7 on 2021-09-26 17:33

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('To', models.CharField(max_length=50)),
                ('From', models.CharField(max_length=50)),
                ('Title', models.CharField(max_length=100)),
                ('Message', models.CharField(max_length=500)),
            ],
        ),
    ]
