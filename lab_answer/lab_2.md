1. Apakah perbedaan antara JSON dan XML?
JSON merupakan format data dan JSON (Javascript Object Notation) digunakan untuk menyimpan data, format pertukaran data yang lebih ringan, dan menggunakan bahasa pemrograman javascript. Data JSON disimpan dalam map (key dan value). Terdapat tipe data yang dapat digunakan di JSON yaitu number, string, array, null, boolean, dan array. Data pada JSON dapat dengan mudah dibaca dan dikonversikan ke javascript. Kecepatannya cukup cepat karena ukuran file kecil sehingga penguraian oleh mesin terjadi lebih cepat.

XML (Extensible Markup Language) merupakan bahasa markup dan XML digunakan untuk mengangkut data melalui internet. Bedanya, XML tidak menggunakan bahasa pemrograman dan dapat dibaca oleh mesin serta manusia. Data XML disimpan menjadi tree structure. Kecepatannya besar dan lambat sehingga transmisi data lebih lambat.

2. Apakah perbedaan antara HTML dan XML?
XML sendiri menyediakan framework/kerangka untuk penentuan bahasa markup sementara HTML merupakan bahasa markup standar. Tujuan keduanya juga berbeda yaitu HTML berfokus pada penyajian data sementara XML berfokus pada transfer informasi. Adanya error kecil pada HTML dapat diabaikan tetapi tidak dengan XML. Segi nesting atau pengurutan dalam HTML tidak perlu diperhitungkan tetapi pada XML harus dengan benar. XML dikatakan lebih case sensitive terhadap besar atau kecil kalimat sementara HTML terbilang case insensitive atau tidak peka terhadap besar kecil kalimat. XML dapat mempertahankan spasi dan harus menggunakan tag penutup sementara HTML tidak dapat mempertahankan spasi dan adanya tag penutup bersifat optional.

Referensi:
https://www.monitorteknologi.com/perbedaan-json-dan-xml/ 
https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html 