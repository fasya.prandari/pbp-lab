import 'dart:js';

import 'package:flutter/material.dart';

import '../widgets/main_drawer.dart';
import 'InputWrapper.dart';

class AboutScreen extends StatefulWidget {
  static const routeName = '/about';

  @override
  _AboutScreen createState() => _AboutScreen();
}

// void kirimdata() {
//   AlertDialog alertDialog = new AlertDialog(
//     content: new Container(
//       height: 200.0,
//       child: new Column(
//         children: <Widget>[
//           new Text("Thank you for your suggestions!"),
//         ],
//       ),
//     ),
//   );
//   showDialog(context: context, child: alertDialog);
// }

class _AboutScreen extends State<AboutScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Login")),
      drawer: MainDrawer(),
      body: Container(
        child: Column(
          children: <Widget>[
            // SizedBox(
            //   height: 80,
            // ),
            // Header(),
            Expanded(
                child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                // borderRadius: BorderRadius.only(
                //   topLeft: Radius.circular(60),
                //   topRight: Radius.circular(60),
                // )
              ),
              child: InputWrapper(),
            ))
          ],
        ),
      ),
    );
  }
}
//   }
// }
