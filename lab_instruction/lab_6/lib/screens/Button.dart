import 'package:flutter/material.dart';

showAlertDialog(BuildContext context) {
  // Create button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );

  // Create AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Login Successful!"),
    // content: Text(""),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class Button extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      margin: EdgeInsets.symmetric(horizontal: 50),
      // decoration: BoxDecoration(
      //   color: Colors.cyan[500],
      //   borderRadius: BorderRadius.circular(10),
      // ),
      child: RaisedButton(
        padding: EdgeInsets.symmetric(vertical: 8, horizontal: 30),
        onPressed: () {
          showAlertDialog(context);
        },
        color: Colors.blue,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Text(
          "Submit",
          style: TextStyle(color: Colors.white),
        ),
      ),

    );
  }
}
  
